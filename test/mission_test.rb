require_relative 'test_helper'

describe Mission do
  describe '#execute' do
    it 'should print rover destinations' do
      mission = Mission.new('4 4', '2')
      mission.upper_x_ordinate.must_equal 4
      mission.upper_y_ordinate.must_equal 4
      mission.navigate_rover('1 2 N', 'LMLMLMLMM').must_equal '1 3 N'
    end
  end
end