require_relative 'test_helper'

describe Rover do
  describe '#destination' do
    it 'should return destination co-ordinates - example 1' do
      rover = Rover.new('1 2 N', 'LMLMLMLMM', Mission.new('5 5', '2'))
      rover.destination.must_equal '1 3 N'
    end

    it 'should return destination co-ordinates - example 2' do
      rover = Rover.new('3 3 E', 'MMRMMRMRRM', Mission.new('5 5', '2'))
      rover.destination.must_equal '5 1 E'
    end
  end
end