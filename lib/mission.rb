require_relative 'rover'

class Mission
  attr_reader :upper_x_ordinate, :upper_y_ordinate, :no_of_rovers

  def initialize(coordinates, no_of_rovers)
    @no_of_rovers = no_of_rovers.to_i
    @coordinates = process_coordinates(coordinates)
  end

  def navigate_rover(rover_ordinates, navigation)
    Rover.new(rover_ordinates, navigation, self).destination
  end

  private

  def process_coordinates(values)
    new_values = values.split(' ')
    @upper_x_ordinate = new_values.first.to_i
    @upper_y_ordinate = new_values.last.to_i
  end
end