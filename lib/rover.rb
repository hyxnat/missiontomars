class Rover
  DIRECTIONS = { n: { l: :w , r: :e }, s: { l: :e, r: :w }, e: { l: :n, r: :s }, w: { l: :s, r: :n } }

  def initialize(ordinates, navigation, mission)
    @ordinates = process_ordinates(ordinates)
    @navigation = navigation.split('')
    @mission = mission
  end

  def destination
    @navigation.each do |c|
      move_one_step if c == 'M'
      turn_left if c == 'L'
      turn_right if c == 'R'        
    end
    "#{@current_x} #{@current_y} #{@current_direction.upcase}"
  end

  private

  def move_one_step
    case @current_direction
    when :n 
      @current_y += 1 unless @current_y == @mission.upper_y_ordinate
    when :s
      @current_y -= 1 unless @current_y == 0
    when :e
      @current_x += 1 unless @current_x == @mission.upper_x_ordinate
    else
      @current_x -= 1 unless @current_x == 0
    end
  end

  def turn_left
    @current_direction = DIRECTIONS[@current_direction][:l]
  end

  def turn_right
    @current_direction = DIRECTIONS[@current_direction][:r]
  end

  def process_ordinates(ordinates)
    new_ordinates = ordinates.split(' ')
    @current_x = new_ordinates.first.to_i
    @current_y = new_ordinates[1].to_i
    @current_direction = new_ordinates.last.downcase.to_sym
  end
end